const disableGraphql = process.env.CI || process.env.NO_GRAPHQL;
const graphqlRules = disableGraphql
  ? {}
  : {
      'graphql/required-fields': [
        'error',
        {
          requiredFields: ['id'],
        },
      ],
      'graphql/named-operations': ['warn'],
      'graphql/capitalized-type-name': ['warn'],
      'graphql/no-deprecated-fields': ['error'],
    };
module.exports = {
  extends: ['airbnb', 'prettier'],
  plugins: disableGraphql ? ['prettier'] : ['prettier', 'graphql'],
  parser: 'babel-eslint',
  rules: {
    'prettier/prettier': ['warn'],
    'react/jsx-wrap-multilines': [
      'error',
      {
        declaration: false,
        assignment: false,
      },
    ],
    'no-console': 'warn',
    ...graphqlRules,
  },
  env: {
    browser: true,
  },
};
