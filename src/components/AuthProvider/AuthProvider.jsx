import React, { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import localforage from 'localforage';
import { SWRConfig } from 'swr';

import { AuthContext } from '../../contexts';
import { gitlabUrl, pollInterval } from '../../config';
import AuthRequired from './AuthRequired';
import { usePersistentReducer } from '../../hooks';

// Auth State ENUM
const S = {
  REQUIRES_AUTH: 'REQUIRES_AUTH',
  AUTHENTICATED: 'AUTHENTICATED',
};

// Auth Action ENUM
const A = {
  DEHYDRATE: 'DEHYDRATE',
  AUTH: 'AUTH',
};

const initialState = {
  stage: S.REQUIRES_AUTH,
  token: null,
};

const authReducer = (state, action) => {
  switch (action.type) {
    case A.AUTH:
      return { stage: S.AUTHENTICATED, token: action.token };
    case A.DEHYDRATE:
      return { stage: action.token ? S.AUTHENTICATED : S.REQUIRES_AUTH, token: action.token };
    default:
      throw new Error(`Invalid action.type in authReducer "${action.type}"`);
  }
};

const AuthProvider = ({ children: childrenProp }) => {
  const [state, dispatch, loading] = usePersistentReducer('auth', authReducer, initialState);
  const { token, stage } = state;
  const fetcher = useCallback(
    (url) =>
      fetch(`${gitlabUrl}${url}`, {
        mode: 'cors',
        headers: {
          authorization: token ? `Bearer ${token}` : '',
        },
      }).then((resp) => resp.json()),
    [token],
  );
  useEffect(() => {
    let inFlight = true;
    if (!loading && window.location.pathname === '/oauth') {
      const searchParams = new URLSearchParams(window.location.hash.slice(1));
      const accessToken = searchParams.get('access_token');
      localforage.setItem('access_token', accessToken).then(() => {
        if (inFlight) {
          dispatch({ type: A.AUTH, token: accessToken });
          window.history.replaceState({}, 'GL Dashboard', '/');
        }
      });
      // On unmount, don't set state
      return () => {
        inFlight = false;
      };
    }
    return () => {};
  }, [loading, dispatch]);
  const children = {
    [S.AUTHENTICATED]: childrenProp,
    [S.REQUIRES_AUTH]: <AuthRequired />,
  }[stage];
  return (
    <AuthContext.Provider value={state}>
      <SWRConfig value={{ fetcher, refreshInterval: pollInterval }}>{children}</SWRConfig>
    </AuthContext.Provider>
  );
};

AuthProvider.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)])
    .isRequired,
};

export default AuthProvider;
