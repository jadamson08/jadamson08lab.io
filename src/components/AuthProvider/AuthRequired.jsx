import React from 'react';
import { Box, Button, Link, Typography } from '@material-ui/core';
import { oauthAppId } from '../../config';

const AuthRequired = () => {
  const CALLBACK = `${document.location.origin}/oauth`;
  const GITLAB_OAUTH = 'https://gitlab.com/oauth/authorize';
  const OAUTH_PARAMS = new URLSearchParams([
    ['client_id', oauthAppId],
    ['redirect_uri', CALLBACK],
    ['response_type', 'token'],
    ['state', 'authing'],
    ['scope', 'api'],
  ]);
  const OAUTH_URL = `${GITLAB_OAUTH}?${OAUTH_PARAMS.toString()}`;
  return (
    <Box
      height="100vh"
      display="flex"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
    >
      <Typography variant="h3">Authentication Required</Typography>
      <Box m={2} />
      <Typography variant="subtitle1">
        This app will store a GitLab auth token locally in your browser
      </Typography>
      <Box m={2} />
      <Button href={OAUTH_URL} variant="contained" color="primary" component="a">
        Gitlab Login
      </Button>
      <Box m={1} />
      <Typography variant="caption">
        {'Note: If you are uncomfortable with trusting this app, feel free to '}
        <Link
          href="https://gitlab.mrfluffybunny.com/jadamson08/gitlab-dashboard"
          target="_blank"
          rel="noopener noreferrer"
        >
          clone it
        </Link>
        ,&nbsp;review it and run it locally. Provided it&apos;s run at http://localhost:3000, it
        should work.
      </Typography>
    </Box>
  );
};

export default AuthRequired;
