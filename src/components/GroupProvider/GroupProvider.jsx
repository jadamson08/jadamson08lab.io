import React from 'react';
import PropTypes from 'prop-types';

import { GroupContext } from '../../contexts';
import { usePersistentState } from '../../hooks';

const GroupProvider = ({ children }) => {
  const [group, setGroup] = usePersistentState('groups', null);
  return <GroupContext.Provider value={{ group, setGroup }}>{children}</GroupContext.Provider>;
};
GroupProvider.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)])
    .isRequired,
};

export default GroupProvider;
