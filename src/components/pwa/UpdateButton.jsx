import React, { useEffect, useState } from 'react';
import { IconButton, Tooltip } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import UpdateIcon from '@material-ui/icons/GetApp';
import { appUpdateInterval } from '../../config';
import * as serviceWorker from '../../serviceWorkerRegistration';

const useStyles = makeStyles((theme) => ({
  button: {
    color: theme.palette.success.main,
  },
}));

const checkUpdate = () => {
  if (process.env.NODE_ENV === 'production' && 'serviceWorker' in navigator) {
    const swUrl = `${process.env.PUBLIC_URL}/service-worker.js`;
    navigator.serviceWorker
      .register(swUrl)
      .then((registration) => registration.update())
      // eslint-disable-next-line no-console
      .catch((err) => console.error('Error updating service-worker: ', err));
  }
};

const UpdateButton = () => {
  const classes = useStyles();
  const [showReload, setShowReload] = useState(false);
  const [waitingWorker, setWaitingWorker] = useState(null);
  const [tooltip, setTooltip] = useState(true);

  const onSWUpdate = (registration) => {
    setWaitingWorker(registration.waiting);
    setShowReload(true);
  };

  useEffect(() => {
    serviceWorker.registerInner({ onUpdate: onSWUpdate });
  }, []);

  useEffect(() => {
    const interval = setInterval(checkUpdate, appUpdateInterval);
    return () => clearInterval(interval);
  }, []);

  const reloadPage = () => {
    if (waitingWorker) waitingWorker.postMessage({ type: 'SKIP_WAITING' });
    setShowReload(false);
    window.location.reload();
  };
  return (
    showReload && (
      <Tooltip
        title="New Version Available"
        aria-label="update app"
        placement="bottom-end"
        arrow
        open={tooltip}
        onOpen={() => setTooltip(true)}
        onClose={() => setTooltip(false)}
      >
        <IconButton
          edge="start"
          className={classes.button}
          aria-label="update app"
          onClick={reloadPage}
        >
          <UpdateIcon />
        </IconButton>
      </Tooltip>
    )
  );
};

export default UpdateButton;
