import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Badge, Box, Tooltip, Typography } from '@material-ui/core';

import { gitlabUrl } from '../../config';

const useStyles = makeStyles((theme) => ({
  groupIcon: {
    width: '32px',
    height: '32px',
  },
  badgeTick: {
    backgroundColor: theme.palette.success.main,
    fontWeight: 'bolder',
  },
}));

const getAvatarUrl = (url) => (url.startsWith('http') ? url : `${gitlabUrl}${url}`);

const AvatarCell = ({ avatarUrl, name, classes, bottomBadge, topBadge }) => {
  const defaultClasses = useStyles();
  return (
    <Tooltip title={name}>
      <Box
        width="40px"
        height="40px"
        bgcolor="#fff"
        borderRadius={4}
        border={2}
        borderColor="primary.light"
        display="flex"
        alignItems="center"
        justifyContent="center"
        mr={1}
      >
        <Badge
          badgeContent={topBadge}
          color="primary"
          anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
        >
          <Badge
            badgeContent={bottomBadge}
            anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
            classes={{ badge: classes.badgeTick || defaultClasses.badgeTick }}
          >
            {avatarUrl ? (
              <img
                src={getAvatarUrl(avatarUrl)}
                alt={`${name} icon`}
                className={classes.groupIcon || defaultClasses.groupIcon}
              />
            ) : (
              <Typography color="primary" variant="h4">
                {name[0]}
              </Typography>
            )}
          </Badge>
        </Badge>
      </Box>
    </Tooltip>
  );
};
AvatarCell.defaultProps = {
  avatarUrl: null,
  classes: {},
  bottomBadge: null,
  topBadge: null,
};
const badgeProp = PropTypes.oneOfType([PropTypes.string, PropTypes.number]);
AvatarCell.propTypes = {
  avatarUrl: PropTypes.string,
  name: PropTypes.string.isRequired,
  classes: PropTypes.shape({
    badgeTick: PropTypes.string,
    groupIcon: PropTypes.string,
  }),
  bottomBadge: badgeProp,
  topBadge: badgeProp,
};
export default AvatarCell;
