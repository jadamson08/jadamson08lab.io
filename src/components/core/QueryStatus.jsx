import React from 'react';
import PropTypes from 'prop-types';
import { Box, Tooltip } from '@material-ui/core';
import ErrorIcon from '@material-ui/icons/ErrorOutline';
import { ClipLoader } from 'react-spinners';

const QueryStatus = ({ data, error, loading }) => {
  if (loading)
    return (
      <>
        <Box m={1} />
        <ClipLoader color="#fff" size="20px" />
      </>
    );
  if (error)
    return (
      <>
        <Box m={1} />
        <Tooltip
          title={`Connectivity error${data ? ', using old data' : ''}`}
          placement="right"
          arrow
        >
          <ErrorIcon color="error" />
        </Tooltip>
      </>
    );
  return null;
};

QueryStatus.defaultProps = {
  data: null,
  error: null,
};

QueryStatus.propTypes = {
  data: PropTypes.shape({}),
  error: PropTypes.shape({}),
  loading: PropTypes.bool.isRequired,
};

export default QueryStatus;
