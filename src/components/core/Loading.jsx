import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { ClipLoader } from 'react-spinners';

const Loading = () => (
  <Box
    display="flex"
    alignItems="center"
    justifyContent="center"
    flexDirection="column"
    height="100%"
  >
    <Typography variant="h4">Loading</Typography>
    <Box m={2} />
    <ClipLoader color="#fff" />
  </Box>
);

export default Loading;
