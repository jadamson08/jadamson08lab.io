import React from 'react';
import { Box, TableCell, TableRow } from '@material-ui/core';
import ClipLoader from 'react-spinners/ClipLoader';

const LoadingRow = () => (
  <TableRow>
    <TableCell colSpan={6}>
      <Box m={2} display="flex" justifyContent="center">
        <ClipLoader color="#fff" />
      </Box>
    </TableCell>
  </TableRow>
);

export default LoadingRow;
