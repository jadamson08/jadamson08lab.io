import React from 'react';
import { useQuery } from '@apollo/client';
import * as R from 'ramda';
import { Box, Typography } from '@material-ui/core';
import { AUTHORED_MERGE_REQUESTS } from './queries';
import MergeRequestTable from './MergeRequestTable';
import { QueryStatus } from '../core';
import { pollInterval } from '../../config';

const AuthoredMergeRequests = () => {
  const { data, loading, error } = useQuery(AUTHORED_MERGE_REQUESTS, {
    pollInterval,
  });
  const authoredMergeRequests = R.path(['currentUser', 'authoredMergeRequests', 'nodes'], data);
  return (
    <>
      <Box display="flex" alignItems="center">
        <Typography variant="h4">My MRs</Typography>
        <QueryStatus loading={loading} error={error} data={data} />
      </Box>
      <Box m={2} />
      <MergeRequestTable mergeRequests={authoredMergeRequests} />
    </>
  );
};

export default AuthoredMergeRequests;
