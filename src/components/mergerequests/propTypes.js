import PropTypes from 'prop-types';

const connection = (inner) =>
  PropTypes.shape({ nodes: PropTypes.arrayOf(PropTypes.shape(inner)).isRequired });
export const ProjectPropType = {
  avatarUrl: PropTypes.string,
  fullPath: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  webUrl: PropTypes.string.isRequired,
};

export const MergeRequestPropType = {
  approvedBy: connection({ id: PropTypes.string.isRequired, name: PropTypes.string.isRequired })
    .isRequired,
  assignees: connection({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    avatarUrl: PropTypes.string,
  }).isRequired,
  author: PropTypes.shape({
    id: PropTypes.string.isRequired,
  }).isRequired,
  reviewers: connection({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    avatarUrl: PropTypes.string,
  }).isRequired,
  project: PropTypes.shape(ProjectPropType).isRequired,
  id: PropTypes.string.isRequired,
  iid: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired,
  updatedAt: PropTypes.string.isRequired,
  downvotes: PropTypes.number.isRequired,
  upvotes: PropTypes.number.isRequired,
  webUrl: PropTypes.string.isRequired,
  workInProgress: PropTypes.bool.isRequired,
  conflicts: PropTypes.bool.isRequired,
  headPipeline: PropTypes.shape({
    active: PropTypes.bool.isRequired,
    status: PropTypes.string.isRequired,
  }),
};
