import MergeRequest from './MergeRequest';
import AuthoredMergeRequests from './AuthoredMergeRequests';
import AssignedMergeRequests from './AssignedMergeRequests';

export { MergeRequest, AuthoredMergeRequests, AssignedMergeRequests };
