import React from 'react';
import * as R from 'ramda';
import { useQuery } from '@apollo/client';
import { Box, Typography } from '@material-ui/core';
import { ASSIGNED_MERGE_REQUESTS, REVIEW_REQUESTED_MERGE_REQUESTS } from './queries';
import MergeRequestTable from './MergeRequestTable';
import { QueryStatus } from '../core';
import { pollInterval } from '../../config';

const AssignedMergeRequests = () => {
  const assigned = useQuery(ASSIGNED_MERGE_REQUESTS, {
    pollInterval,
  });
  const reviewing = useQuery(REVIEW_REQUESTED_MERGE_REQUESTS, {
    pollInterval,
  });
  const loading = assigned.loading || reviewing.loading;
  const error = assigned.error || reviewing.error;
  const data = assigned.data || reviewing.data;
  const userId = R.path(['currentUser', 'id'], assigned.data);
  const assignedMergeRequests = R.path(
    ['currentUser', 'assignedMergeRequests', 'nodes'],
    assigned.data,
  );
  const assignedMergeRequestsArr = assignedMergeRequests || [];
  const reviewingMergeRequests = R.path(
    ['currentUser', 'reviewRequestedMergeRequests', 'nodes'],
    reviewing.data,
  );
  const reviewingMergeRequestsArr = reviewingMergeRequests || [];
  const mergeRequests = !(assignedMergeRequests || reviewingMergeRequests)
    ? null
    : [...reviewingMergeRequestsArr, ...assignedMergeRequestsArr];
  const mergeRequestsNotAuthored = mergeRequests.filter(
    R.complement(R.pathEq(['author', 'id'], userId)),
  );
  const mergeRequestsUniq = R.uniqBy(R.prop('id'), mergeRequestsNotAuthored);
  return (
    <>
      <Box display="flex" alignItems="center">
        <Typography variant="h4">To Review</Typography>
        <QueryStatus loading={loading} error={error} data={data} />
      </Box>
      <Box m={2} />
      <MergeRequestTable mergeRequests={mergeRequestsUniq} />
    </>
  );
};

export default AssignedMergeRequests;
