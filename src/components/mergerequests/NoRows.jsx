import React from 'react';
import { Box, TableCell, TableRow, Typography } from '@material-ui/core';

const NoRows = () => (
  <TableRow>
    <TableCell colSpan={6}>
      <Box m={2} display="flex" justifyContent="center">
        <Typography variant="subtitle2">No open merge requests</Typography>
      </Box>
    </TableCell>
  </TableRow>
);

export default NoRows;
