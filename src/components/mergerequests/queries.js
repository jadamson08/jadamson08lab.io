import { gql } from '@apollo/client';
import { PAGE_INFO } from '../../queries/fragments';

export const MERGE_REQUEST_FIELDS = gql`
  fragment MergeRequestFields on MergeRequest {
    assignees {
      nodes {
        id
        avatarUrl
        name
      }
    }
    reviewers {
      nodes {
        id
        avatarUrl
        name
      }
    }
    author {
      id
      avatarUrl
      name
    }
    project {
      id
      fullPath
      avatarUrl
      name
      webUrl
    }
    approvedBy {
      nodes {
        id
        name
      }
    }
    headPipeline {
      id
      active
      status
    }
    webUrl
    title
    id
    iid
    updatedAt
    createdAt
    upvotes
    approved
    conflicts
    downvotes
    workInProgress
  }
`;

export const ASSIGNED_MERGE_REQUESTS = gql`
  query GetAssignedMRs {
    currentUser {
      id
      name
      assignedMergeRequests(state: opened, sort: UPDATED_DESC) {
        nodes {
          id
          ...MergeRequestFields
        }
        pageInfo {
          ...PageInfoFields
        }
      }
    }
  }
  ${PAGE_INFO}
  ${MERGE_REQUEST_FIELDS}
`;
export const REVIEW_REQUESTED_MERGE_REQUESTS = gql`
  query GetReviewRequestedMRs {
    currentUser {
      id
      name
      reviewRequestedMergeRequests(state: opened, sort: UPDATED_DESC) {
        nodes {
          id
          ...MergeRequestFields
        }
        pageInfo {
          ...PageInfoFields
        }
      }
    }
  }
  ${PAGE_INFO}
  ${MERGE_REQUEST_FIELDS}
`;
export const AUTHORED_MERGE_REQUESTS = gql`
  query GetAssignedMRs {
    currentUser {
      id
      authoredMergeRequests(state: opened, sort: UPDATED_DESC) {
        nodes {
          id
          ...MergeRequestFields
        }
        pageInfo {
          ...PageInfoFields
        }
      }
    }
  }
  ${PAGE_INFO}
  ${MERGE_REQUEST_FIELDS}
`;
export const GET_MERGE_REQUEST_COMMENTS = gql`
  query GetMergeRequestComments($project: ID!, $mergeRequest: String!) {
    project(fullPath: $project) {
      id
      mergeRequest(iid: $mergeRequest) {
        id
        iid
        discussions {
          nodes {
            id
            resolved
            resolvable
            notes {
              nodes {
                id
                body
                updatedAt
                author {
                  id
                }
              }
              pageInfo {
                ...PageInfoFields
              }
            }
          }
          pageInfo {
            ...PageInfoFields
          }
        }
      }
    }
  }
  ${PAGE_INFO}
`;
