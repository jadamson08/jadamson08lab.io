/* eslint-disable no-nested-ternary */
import React from 'react';
import PropTypes from 'prop-types';
import useSWR from 'swr';
import moment from 'moment';
import * as R from 'ramda';
import { useQuery } from '@apollo/client';
import { Box, Link, TableCell, TableRow, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { GET_MERGE_REQUEST_COMMENTS } from './queries';
import { AvatarCell } from '../core';
import { pollInterval } from '../../config';
import { MergeRequestPropType } from './propTypes';
import MergeRequestStatus from './MergeRequestStatus';

const useStyles = makeStyles((theme) => ({
  groupIcon: {
    width: '32px',
    height: '32px',
  },
  link: {
    color: theme.palette.text.primary,
  },
  workInProgress: {
    filter: 'brightness(50%)',
  },
}));
const MergeRequest = ({ mr }) => {
  const classes = useStyles();
  const {
    author: { id: authorId },
    approvedBy,
    webUrl,
    updatedAt,
    createdAt,
    project,
    title,
    iid,
    workInProgress,
    assignees: { nodes: assignees },
    reviewers: { nodes: reviewers },
  } = mr;
  const allReviewers = R.uniqBy(R.prop('id'), [...assignees, ...reviewers]).filter(
    ({ id }) => id !== authorId,
  );
  const projectId = project.id.replace('gid://gitlab/Project/', '');
  const { data: awards = [] } = useSWR(
    `/api/v4/projects/${projectId}/merge_requests/${iid}/award_emoji`,
  );
  const upvoteUserIds = awards
    .filter(R.propEq('name', 'thumbsup'))
    .map(({ user }) => `gid://gitlab/User/${user.id}`);
  const approvedUserIds = approvedBy.nodes.map(R.prop('id'));
  const { data: commentData } = useQuery(GET_MERGE_REQUEST_COMMENTS, {
    variables: { project: project.fullPath, mergeRequest: iid },
    pollInterval,
  });
  const discussions = commentData ? commentData.project.mergeRequest.discussions.nodes : [];
  const unresolvedDiscussions = discussions.filter(
    ({ resolved, resolvable }) => !resolved && resolvable,
  );
  const unresolvedCount = commentData ? unresolvedDiscussions.length : null;
  const unresolvedUserSets = unresolvedDiscussions.flatMap(({ notes: { nodes } }) =>
    R.uniq(nodes.map(({ author }) => author.id)),
  );
  const userUnresolvedCount = R.countBy(R.identity)(unresolvedUserSets);
  return (
    <TableRow classes={{ root: workInProgress && classes.workInProgress }}>
      <TableCell>
        <Box display="flex" alignItems="center">
          <AvatarCell avatarUrl={project.avatarUrl} name={project.name} classes={classes} />
          <Typography
            component={Link}
            variant="subtitle1"
            href={webUrl}
            target="_blank"
            rel="noopener noreferrer"
            className={classes.link}
          >
            {project.name}
            <Typography variant="caption" component="span">{` !${iid}`}</Typography>
          </Typography>
        </Box>
      </TableCell>
      <TableCell>
        <Link href={webUrl} className={classes.link} target="_blank" rel="noopener noreferrer">
          {title}
        </Link>
      </TableCell>
      <TableCell align="center">
        <MergeRequestStatus mr={mr} unresolvedCount={unresolvedCount} discussions={discussions} />
      </TableCell>
      <TableCell align="right">{moment(createdAt).fromNow(true)}</TableCell>
      <TableCell align="right">{moment(updatedAt).fromNow(true)}</TableCell>
      <TableCell align="right">
        <Box display="flex" justifyContent="center">
          {allReviewers.map(({ id, name, avatarUrl }) => (
            <AvatarCell
              avatarUrl={avatarUrl}
              name={name}
              key={id}
              classes={classes}
              bottomBadge={
                approvedUserIds.includes(id) ? '✓' : upvoteUserIds.includes(id) ? '👍' : null
              }
              topBadge={userUnresolvedCount[id]}
            />
          ))}
        </Box>
      </TableCell>
    </TableRow>
  );
};
MergeRequest.propTypes = {
  mr: PropTypes.shape(MergeRequestPropType).isRequired,
};
export default MergeRequest;
