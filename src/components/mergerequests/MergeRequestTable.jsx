import React from 'react';
import PropTypes from 'prop-types';
import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@material-ui/core';
import { MergeRequestPropType } from './propTypes';
import MergeRequest from './MergeRequest';
import LoadingRow from './LoadingRow';
import NoRows from './NoRows';

const MergeRequestTable = ({ mergeRequests }) => {
  const fillerRow = mergeRequests ? <NoRows /> : <LoadingRow />;
  return (
    <TableContainer component={Paper}>
      <Table aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Project</TableCell>
            <TableCell>Name</TableCell>
            <TableCell align="center">Status</TableCell>
            <TableCell align="right">Created</TableCell>
            <TableCell align="right">Updated</TableCell>
            <TableCell align="center">Approvals</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {mergeRequests && mergeRequests.length > 0
            ? mergeRequests.map((mr) => <MergeRequest mr={mr} key={mr.id} />)
            : fillerRow}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
MergeRequestTable.defaultProps = {
  mergeRequests: null,
};
MergeRequestTable.propTypes = {
  mergeRequests: PropTypes.arrayOf(PropTypes.shape(MergeRequestPropType)),
};

export default MergeRequestTable;
