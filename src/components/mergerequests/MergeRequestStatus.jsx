import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Chip } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { MergeRequestPropType } from './propTypes';
import debug from '../../debug';

const getStatus = (mr, unresolvedCount) => {
  if (mr.workInProgress) return ['Draft', 'chipGrey'];
  if (mr.downvotes) return ['Downvoted', 'chipError'];
  if (R.pathEq(['headPipeline', 'status'], 'FAILED')(mr)) return ['Failed', 'chipError'];
  if (mr.conflicts) return ['Merge Conflict', 'chipWarning'];
  if (mr.upvotes === 2) return [`Approved${mr.approved ? '' : '?'}`, 'chipSuccess'];
  if (unresolvedCount > 0) return ['Unresolved', 'chipPrimary'];
  if (unresolvedCount === 0) return ['Review', 'chipSecondary'];
  return ['Loading', 'chipGrey'];
};
const useStyles = makeStyles((theme) => ({
  chipSuccess: {
    backgroundColor: theme.palette.success.main,
    color: theme.palette.success.contrastText,
  },
  chipWarning: {
    backgroundColor: theme.palette.warning.main,
    color: theme.palette.warning.contrastText,
  },
  chipError: {
    backgroundColor: theme.palette.error.main,
    color: theme.palette.error.contrastText,
  },
  chipPrimary: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
  },
  chipSecondary: {
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
  },
  chipGrey: {
    backgroundColor: theme.palette.grey['600'],
  },
}));

const MergeRequestStatus = ({ mr, unresolvedCount, discussions }) => {
  debug.log({ discussions });
  const classes = useStyles();
  const [label, stateClass] = getStatus(mr, unresolvedCount);
  return <Chip label={label} className={classes[stateClass]} />;
};
MergeRequestStatus.defaultProps = {
  unresolvedCount: null,
};
MergeRequestStatus.propTypes = {
  discussions: PropTypes.arrayOf(
    PropTypes.shape({
      // notes: PropTypes.array.isRequired,
    }),
  ).isRequired,
  mr: PropTypes.shape(MergeRequestPropType).isRequired,
  unresolvedCount: PropTypes.number,
};

export default MergeRequestStatus;
