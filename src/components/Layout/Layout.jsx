import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { TitleContext } from '../../contexts';
import TopBar from './TopBar';
import SideNav, { routesPropType } from './SideNav';

const useStyles = makeStyles((theme) => ({
  toolbar: theme.mixins.toolbar,
  content: {
    width: '100%',
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(1),
    minHeight: 'calc(100vh - 64px)',
  },
  outerMain: {
    width: '100%',
    minHeight: '100vh',
  },
}));

const Layout = ({ children, routes }) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [title, setTitle] = useState('GL Dashboard');
  const handleToggle = () => setOpen((val) => !val);
  return (
    <TitleContext.Provider value={{ title, setTitle }}>
      <Box display="flex" width="100%">
        <TopBar open={open} handleToggle={handleToggle} />
        <SideNav open={open} handleToggle={handleToggle} routes={routes} />
        <div className={classes.outerMain}>
          <div className={classes.toolbar} />
          <main className={classes.content}>{children}</main>
        </div>
      </Box>
    </TitleContext.Provider>
  );
};

Layout.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)])
    .isRequired,
  routes: routesPropType.isRequired,
};

export default Layout;
