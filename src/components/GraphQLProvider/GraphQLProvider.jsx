import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { ApolloClient, ApolloProvider, createHttpLink, InMemoryCache } from '@apollo/client';
import { LocalStorageWrapper, CachePersistor } from 'apollo3-cache-persist';
import { setContext } from '@apollo/client/link/context';
import { graphqlUrl } from '../../config';
import { AuthContext } from '../../contexts';
import { Loading } from '../core';

const GraphQlProvider = ({ children }) => {
  const { token } = useContext(AuthContext);
  const httpLink = createHttpLink({
    uri: graphqlUrl,
  });
  const [client, setClient] = useState();
  useEffect(() => {
    async function init() {
      const cache = new InMemoryCache();
      const newPersistor = new CachePersistor({
        cache,
        storage: new LocalStorageWrapper(window.localStorage),
        debug: true,
        trigger: 'write',
      });
      const authLink = setContext(async (_, { headers }) =>
        // return the headers to the context so httpLink can read them
        ({
          headers: {
            ...headers,
            authorization: token ? `Bearer ${token}` : '',
          },
        }),
      );
      await newPersistor.restore();
      setClient(
        new ApolloClient({
          link: authLink.concat(httpLink),
          cache,
          defaultOptions: {
            watchQuery: {
              fetchPolicy: 'cache-and-network',
            },
          },
        }),
      );
    }
    // eslint-disable-next-line no-console
    init().catch(console.error);
  }, [token]);

  if (!client) return <Loading />;
  return <ApolloProvider client={client}>{children}</ApolloProvider>;
};

GraphQlProvider.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)])
    .isRequired,
};

export default GraphQlProvider;
