import React from 'react';
import ReactDOM from 'react-dom';
import { Box, CssBaseline } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/core/styles';
import { BrowserRouter as Router } from 'react-router-dom';
// import * as serviceWorker from './serviceWorkerRegistration';
import 'typeface-roboto';

import Pages from './pages';
import theme from './theme';
import AuthProvider from './components/AuthProvider';
import GraphQlProvider from './components/GraphQLProvider';
import GroupProvider from './components/GroupProvider';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <CssBaseline />
    <Box minHeight="100vh">
      <AuthProvider>
        <GraphQlProvider>
          <GroupProvider>
            <Router>
              <Pages />
            </Router>
          </GroupProvider>
        </GraphQlProvider>
      </AuthProvider>
    </Box>
  </ThemeProvider>,
  document.querySelector('#root'),
);
// serviceWorker.register({onUpdate: console.log});
