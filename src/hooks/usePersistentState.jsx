import { useEffect, useState } from 'react';
import localforage from 'localforage';

const usePersistentState = (key, initialValue) => {
  const [state, setState] = useState(initialValue);
  const [loading, setLoading] = useState(true);

  // Load from localforage and dehydrate on instantiation
  useEffect(() => {
    let mounted = true;
    localforage.getItem(key).then((oldState) => {
      if (mounted) {
        setLoading(false);
        if (oldState) {
          setState(JSON.parse(oldState));
        }
      }
    });
    return () => {
      mounted = false;
    };
  }, [key]);

  // Persist changes to localforage
  useEffect(() => {
    if (!loading) {
      const stateSerialized = JSON.stringify(state);
      localforage.setItem(key, stateSerialized);
    }
  }, [state, loading, key]);
  return [state, setState, loading];
};

export default usePersistentState;
