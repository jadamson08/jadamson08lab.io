import { useReducer, useEffect, useState } from 'react';
import localforage from 'localforage';
import debug from '../debug';

const wrappedReduce = (key, reduceFunc) => (state, action) => {
  const resultingState = action.type === 'DEHYDRATE' ? action.state : reduceFunc(state, action);
  const label = `${key}: ${action.type}`;
  debug.groupCollapsed(label);
  debug.info('Action:', action);
  debug.info('State:', state);
  debug.info('New State:', resultingState);
  debug.groupEnd(label);
  return resultingState;
};

const usePersistentReducer = (key, reduceFunc, ...args) => {
  const reduce = wrappedReduce(key, reduceFunc);
  const [loading, setLoading] = useState(true);
  const [state, dispatch] = useReducer(reduce, ...args);
  // Load from localforage and dehydrate on instantiation
  useEffect(() => {
    let mounted = true;
    localforage.getItem(key).then((oldState) => {
      if (mounted) {
        setLoading(false);
        if (oldState) {
          dispatch({ type: 'DEHYDRATE', state: JSON.parse(oldState) });
        }
      }
    });
    return () => {
      mounted = false;
    };
  }, [key, dispatch]);

  // Persist changes to localforage
  useEffect(() => {
    if (!loading) {
      const stateSerialized = JSON.stringify(state);
      localforage.setItem(key, stateSerialized);
    }
  }, [state, loading, key]);
  return [state, dispatch, loading];
};

export default usePersistentReducer;
