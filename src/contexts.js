import React from 'react';

export const AuthContext = React.createContext({ loaded: false });
export const TitleContext = React.createContext({ title: 'GL Dashboard' });
export const GroupContext = React.createContext({ group: null });
