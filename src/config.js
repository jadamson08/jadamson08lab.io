export const oauthAppId = 'd3e907105a0ab7783f7aec6dc3585a3b68d27738c4167ffd74ce357196e591ca';
export const gitlabUrl = 'https://gitlab.com';
export const graphqlUrl = `${gitlabUrl}/api/graphql/`;
export const pollInterval = 60 * 1000;
export const appUpdateInterval = 60 * 60 * 1000; // How regularly to check for new app version
