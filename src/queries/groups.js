import { gql } from '@apollo/client';
import { PAGE_INFO } from './fragments';

export const LIST_GROUPS = gql`
  query ListGroups {
    currentUser {
      id
      groupMemberships {
        nodes {
          id
          group {
            id
            name
            fullPath
            avatarUrl
            fullName
          }
        }
        pageInfo {
          ...PageInfoFields
        }
      }
    }
  }
  ${PAGE_INFO}
`;

export const LIST_SUBGROUPS = gql`
  query ListSubgroups($fullPath: ID!) {
    group(fullPath: $fullPath) {
      id
      projects(includeSubgroups: true) {
        nodes {
          id
          group {
            id
            path
            fullPath
            name
            fullName
            avatarUrl
          }
        }
        pageInfo {
          ...PageInfoFields
        }
      }
    }
  }
  ${PAGE_INFO}
`;
