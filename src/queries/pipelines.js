import { gql } from '@apollo/client';
import { PAGE_INFO } from './fragments';

// eslint-disable-next-line import/prefer-default-export
export const RUNNING_PIPELINES = gql`
  query GetRunningPipelines($group: ID!) {
    group(fullPath: $group) {
      id
      projects {
        pageInfo {
          hasNextPage
        }
        edges {
          node {
            id
            name
            path
            pipelines(status: RUNNING) {
              nodes {
                id
                createdAt
                updatedAt
                committedAt
                duration
              }
              pageInfo {
                ...PageInfoFields
              }
            }
          }
        }
      }
    }
  }
  ${PAGE_INFO}
`;
