import { gql } from '@apollo/client';

// eslint-disable-next-line import/prefer-default-export
export const PAGE_INFO = gql`
  fragment PageInfoFields on PageInfo {
    startCursor
    endCursor
    hasNextPage
    hasPreviousPage
  }
`;
