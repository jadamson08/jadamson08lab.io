import { Box, Container, Typography } from '@material-ui/core';
import React, { useContext } from 'react';
import { useQuery } from '@apollo/client';
import { useTitle } from '../hooks';
import { RUNNING_PIPELINES } from '../queries/pipelines';
import { GroupContext } from '../contexts';
import debug from '../debug';

const Pipelines = () => {
  useTitle('Pipelines');
  const { group } = useContext(GroupContext);
  const { data } = useQuery(RUNNING_PIPELINES, { variables: { group } });
  debug.log({ data });
  return (
    <Container maxWidth="sm">
      <Box my={4}>
        <Typography variant="h4" component="h1" gutterBottom>
          Hi Im Pipelines
        </Typography>
      </Box>
    </Container>
  );
};

export default Pipelines;
