import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { useQuery } from '@apollo/client';

import { makeStyles } from '@material-ui/core/styles';
import { Box, Container, Typography } from '@material-ui/core';
import TreeItem from '@material-ui/lab/TreeItem';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ClipLoader from 'react-spinners/ClipLoader';

import { useTitle } from '../hooks';
import { LIST_GROUPS, LIST_SUBGROUPS } from '../queries/groups';
import { GroupContext } from '../contexts';

const useStyles = makeStyles(() => ({
  groupIcon: {
    width: '32px',
    height: '32px',
  },
}));

const GroupItem = ({ name, fullName, fullPath, avatarUrl, depth }) => {
  const classes = useStyles();
  return (
    <TreeItem
      nodeId={fullPath}
      label={
        <Box display="flex" alignItems="center" my={1}>
          <Box
            width="40px"
            height="40px"
            bgcolor="#fff"
            borderRadius={4}
            border={2}
            borderColor="primary.light"
            display="flex"
            alignItems="center"
            justifyContent="center"
            mr={1}
          >
            {avatarUrl ? (
              <img src={avatarUrl} alt={`${fullName} icon`} className={classes.groupIcon} />
            ) : (
              <Typography color="primary" variant="h4">
                {name[0]}
              </Typography>
            )}
          </Box>
          <Typography variant="subtitle1" display="inline">
            {fullName}
          </Typography>
        </Box>
      }
    >
      <GroupItemSubgroups fullPath={fullPath} depth={depth + 1} />
    </TreeItem>
  );
};
GroupItem.defaultProps = {
  avatarUrl: null,
  depth: 1,
};
GroupItem.propTypes = {
  name: PropTypes.string.isRequired,
  fullName: PropTypes.string.isRequired,
  fullPath: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string,
  depth: PropTypes.number,
};

const sortSubgroups = (data, depth) =>
  R.compose(
    R.uniqBy(R.prop('fullPath')),
    R.sortBy(R.prop('fullPath')),
    R.filter(({ fullPath }) => fullPath.split('/').length === depth),
    R.map(R.prop('group')),
    R.path(['group', 'projects', 'nodes']),
  )(data);

const GroupItemSubgroups = ({ fullPath, depth }) => {
  const { data, loading } = useQuery(LIST_SUBGROUPS, { variables: { fullPath } });
  const sorted = data ? sortSubgroups(data, depth) : [];
  return (
    <>
      {loading ? (
        <Box m={1}>
          <ClipLoader color="#fff" />
        </Box>
      ) : (
        sorted.map((group) => (
          <GroupItem
            avatarUrl={group.avatarUrl}
            name={group.name}
            fullName={group.fullName}
            fullPath={group.fullPath}
            key={group.fullPath}
            depth={depth}
          />
        ))
      )}
    </>
  );
};
GroupItemSubgroups.propTypes = {
  fullPath: PropTypes.string.isRequired,
  depth: PropTypes.number.isRequired,
};

const sortGroups = R.compose(
  R.uniqBy(R.prop('fullPath')),
  R.sortBy(R.prop('fullPath')),
  R.filter(({ fullPath }) => fullPath.split('/').length === 1),
  R.map(R.prop('group')),
);

const Groups = () => {
  useTitle('Groups');
  const { data, loading } = useQuery(LIST_GROUPS);
  const groups = R.path(['currentUser', 'groupMemberships', 'nodes'], data) || [];
  const sorted = sortGroups(groups);
  const { setGroup } = useContext(GroupContext);
  const onNodeSelect = (e, newGroup) => {
    setGroup(newGroup);
  };
  return (
    <Container maxWidth="sm">
      <Box my={4}>
        <Typography variant="h4" component="h1" gutterBottom>
          Groups
        </Typography>
        {loading ? (
          <Box mt={2} display="flex" flexDirection="column" alignItems="center">
            <Typography variant="h4">Loading...</Typography>
            <Box m={2} />
            <ClipLoader color="#fff" />
          </Box>
        ) : (
          <TreeView
            defaultCollapseIcon={<ExpandMoreIcon />}
            defaultExpandIcon={<ChevronRightIcon />}
            onNodeSelect={onNodeSelect}
          >
            {sorted.map((group) => (
              <GroupItem
                name={group.name}
                avatarUrl={group.avatarUrl}
                fullName={group.fullName}
                fullPath={group.fullPath}
                key={group.fullPath}
              />
            ))}
          </TreeView>
        )}
      </Box>
    </Container>
  );
};

export default Groups;
