import React from 'react';
import { Box } from '@material-ui/core';
import { useTitle } from '../hooks';
import { AssignedMergeRequests, AuthoredMergeRequests } from '../components/mergerequests';

const MergeRequests = () => {
  useTitle('Merge Requests');
  return (
    <Box p={2} width="100%">
      <AuthoredMergeRequests />
      <Box m={4} />
      <AssignedMergeRequests />
    </Box>
  );
};

export default MergeRequests;
