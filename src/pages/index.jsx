import React, { lazy, Suspense } from 'react';
import { Link as RouterLink, Route, Switch } from 'react-router-dom';
import { Box, Container, Link, Typography } from '@material-ui/core';
import DashboardIcon from '@material-ui/icons/Dashboard';
import PipelineIcon from '@material-ui/icons/FastForward';
import GroupIcon from '@material-ui/icons/Group';

import { useTitle } from '../hooks';
// import Pipelines from './pipelines';
import Layout from '../components/Layout';
import { Loading } from '../components/core';

const MergeRequests = lazy(() => import('./mergerequests'));
const Groups = lazy(() => import('./groups'));

const Dashboard = () => {
  useTitle('Dashboard');
  return (
    <Container maxWidth="sm">
      <Box my={4}>
        <Typography variant="h4" component="h1" gutterBottom>
          Example Gitlab View
        </Typography>
        <Typography variant="body1">
          {'To see something remotely useful, perhaps checkout '}
          <Link component={RouterLink} to="/mergerequests">
            merge requests
          </Link>
        </Typography>
      </Box>
    </Container>
  );
};

const routes = [
  { title: 'Dashboard', icon: DashboardIcon, path: '/', exact: true, component: Dashboard },
  { title: 'Groups', icon: GroupIcon, path: '/groups', component: Groups },
  // { title: 'Pipelines', icon: PipelineIcon, path: '/pipelines', component: Pipelines },
  { title: 'Merge Requests', icon: PipelineIcon, path: '/mergerequests', component: MergeRequests },
];

const Index = () => (
  <Layout routes={routes}>
    <Suspense fallback={<Loading />}>
      <Switch>
        {routes.map(({ path, exact, component }) => (
          <Route key={path} component={component} exact={exact} path={path} />
        ))}
      </Switch>
    </Suspense>
  </Layout>
);

export default Index;
